
Imports System.IO



Public Class Form1

    Private Function hilbert(ByVal x As Single, ByVal y As Single, ByVal n As Double) As Double
        Dim xmin As Single
        Dim xmax As Single
        Dim ymin As Single
        Dim ymax As Single
        xmin = Me.TextBox2.Text
        xmax = Me.TextBox3.Text
        ymin = Me.TextBox4.Text
        ymax = Me.TextBox5.Text
        Dim i As Integer = 0
        Dim quad As Integer
        Dim hilbertval As Double
        Dim hillarray(n - 1) As Integer

        If x <= xmin + ((xmax - xmin) / 2) And x >= xmin And y <= ymin + ((ymax - ymin) / 2) And y >= ymin Then
            'left down quarter
            xmax = xmin + ((xmax - xmin) / 2)
            ymax = ymin + ((ymax - ymin) / 2)
            quad = 1
            hillarray(0) = quad
        ElseIf x <= xmax And x >= xmin + ((xmax - xmin) / 2) And y <= ymin + ((ymax - ymin) / 2) And y >= ymin Then
            'right down quarter
            xmin = xmin + ((xmax - xmin) / 2)
            ymax = ymin + ((ymax - ymin) / 2)
            quad = 2
            hillarray(0) = quad
        ElseIf x <= xmin + ((xmax - xmin) / 2) And x >= xmin And y <= ymax And y >= ymin + ((ymax - ymin) / 2) Then
            'left up  quarter
            xmax = xmin + ((xmax - xmin) / 2)
            ymin = ymin + ((ymax - ymin) / 2)
            quad = 4
            hillarray(0) = quad
        ElseIf x <= xmax And x >= xmin + ((xmax - xmin) / 2) And y <= ymax And y >= ymin + ((ymax - ymin) / 2) Then
            'right up  quarter
            xmin = xmin + ((xmax - xmin) / 2)
            ymin = ymin + ((ymax - ymin) / 2)
            quad = 3
            hillarray(0) = quad
        End If
        For i = 1 To n - 1
            If x <= xmin + ((xmax - xmin) / 2) And x >= xmin And y <= ymin + ((ymax - ymin) / 2) And y >= ymin Then
                'left down quarter
                xmax = xmin + ((xmax - xmin) / 2)
                ymax = ymin + ((ymax - ymin) / 2)
                Select Case quad
                    Case 1
                        quad = 1
                        hillarray(i) = quad
                    Case 2
                        quad = 1
                        hillarray(i) = quad
                    Case 3
                        quad = 1
                        hillarray(i) = quad
                    Case 4
                        quad = 3
                        hillarray(i) = quad
                End Select
            ElseIf x <= xmax And x >= xmin + ((xmax - xmin) / 2) And y <= ymin + ((ymax - ymin) / 2) And y >= ymin Then
                'right down quarter
                xmin = xmin + ((xmax - xmin) / 2)
                ymax = ymin + ((ymax - ymin) / 2)
                Select Case quad
                    Case 1
                        quad = 4
                        hillarray(i) = quad
                    Case 2
                        quad = 2
                        hillarray(i) = quad
                    Case 3
                        quad = 2
                        hillarray(i) = quad
                    Case 4
                        quad = 2
                        hillarray(i) = quad
                End Select
            ElseIf x <= xmin + ((xmax - xmin) / 2) And x >= xmin And y <= ymax And y >= ymin + ((ymax - ymin) / 2) Then
                'left up  quarter
                xmax = xmin + ((xmax - xmin) / 2)
                ymin = ymin + ((ymax - ymin) / 2)
                Select Case quad
                    Case 1
                        quad = 2
                        hillarray(i) = quad
                    Case 2
                        quad = 4
                        hillarray(i) = quad
                    Case 3
                        quad = 4
                        hillarray(i) = quad
                    Case 4
                        quad = 4
                        hillarray(i) = quad
                End Select
            ElseIf x <= xmax And x >= xmin + ((xmax - xmin) / 2) And y <= ymax And y >= ymin + ((ymax - ymin) / 2) Then
                'right up  quarter
                xmin = xmin + ((xmax - xmin) / 2)
                ymin = ymin + ((ymax - ymin) / 2)
                Select Case quad
                    Case 1
                        quad = 3
                        hillarray(i) = quad
                    Case 2
                        quad = 3
                        hillarray(i) = quad
                    Case 3
                        quad = 3
                        hillarray(i) = quad
                    Case 4
                        quad = 1
                        hillarray(i) = quad
                End Select
            End If
        Next i
        For i = 0 To n - 1
            hilbertval = hilbertval + (hillarray(i) - 1) * System.Math.Pow(4, n - i - 1)
        Next i
        Return hilbertval
    End Function
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim ioline As String
        Dim tuples(123594) As String
        Dim i As Double = 0
        Dim arraysize As Integer
        Try
            Dim oFile As FileStream = New FileStream(Environment.CurrentDirectory & "\in.txt", FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite)
            Dim oReader As StreamReader = New StreamReader(oFile)
            ioline = "non-empty"
            Do Until ioline Is Nothing
                ioline = oReader.ReadLine
                If ioline = Nothing Then
                    GoTo out
                End If
                Dim hilbertval As Double
                Dim n As Double
                n = CInt(TextBox1.Text)
                Dim x As Single
                Dim y As Single
                Dim strArr() As String
                Dim str As String
                str = ioline
                strArr = str.Split
                x = strArr(0)
                y = strArr(1)
                hilbertval = hilbert(x, y, n)
                'store in the array
                tuples(i) = ioline & " " & hilbertval
                i = i + 1
            Loop
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

out:
        'write down 
        arraysize = i
        Try
            Dim oFile As FileStream = New FileStream(Environment.CurrentDirectory & "\out.txt", FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite)
            Dim iWriter As StreamWriter = New StreamWriter(oFile)
            For i = 0 To arraysize - 1
                iWriter.WriteLine(tuples(i))
            Next i
            MessageBox.Show("End of Process")
            iWriter.Close()
            oFile.Close()

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class
