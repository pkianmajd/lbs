Imports System.IO
Imports System.Security.Cryptography
Public Class Form1

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim ioline As String
        Dim tuples(125595) As String
        Dim i As Integer = 0
        Dim totnum As Integer = 0
        Dim reqnum As Integer = TextBox1.Text
        Dim final_tuples(125595) As String
        Try
            Dim oFile As FileStream = New FileStream(Environment.CurrentDirectory & "\in.txt", FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite)
            Dim oReader As StreamReader = New StreamReader(oFile)

            ioline = "non-empty"   'enetering the do..until loop for the first time
            Do Until ioline Is Nothing
                ioline = oReader.ReadLine
                If ioline Is Nothing Then
                    GoTo 1
                End If
                tuples(i) = ioline
                i = i + 1
                totnum = totnum + 1
            Loop
1:
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        Static rr As Random
        Dim seed() As Byte = New Byte(3) {}
        Dim rng As New RNGCryptoServiceProvider()
        Dim index As Integer
        If totnum < reqnum Then
            reqnum = totnum
        End If
        For i = 0 To reqnum - 1
            rng.GetBytes(seed)
            rr = New Random(BitConverter.ToInt32(seed, 0))
            index = rr.Next(0, totnum)
            While tuples(index) = ""
                index = index + 1
                If index = totnum - 1 Then
                    index = 0
                End If
            End While
            final_tuples(i) = tuples(index)
            tuples(index) = ""
        Next i
        'write down
        Try
            Dim oFile As FileStream = New FileStream(Environment.CurrentDirectory & "\out.txt", FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite)
            Dim iWriter As StreamWriter = New StreamWriter(oFile)
            For i = 0 To reqnum - 1
                iWriter.WriteLine(final_tuples(i))
            Next i
            iWriter.Close()
            oFile.Close()
            MessageBox.Show("End of Process")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class
