Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms
Imports System.IO
Imports System.Security.Cryptography
Imports System.Diagnostics

Public Class Form1
    Inherits System.Windows.Forms.Form

    Public Sub New()
        MyBase.New()

        Form1 = Me

        'This call is required by the Win Form Designer.
        InitializeComponent()

        'TODO: Add any initialization after the InitializeComponent() call
    End Sub

    'Form overrides dispose to clean up the component list.
    Public Overloads Sub Dispose()
        MyBase.Dispose()
        components.Dispose()
    End Sub

#Region " Windows Form Designer generated code "

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.Container
    Private WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label

    Dim WithEvents Form1 As System.Windows.Forms.Form

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(55, 178)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(235, 33)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Generate Health Random Conditions"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(145, 71)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(41, 20)
        Me.TextBox1.TabIndex = 1
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(145, 113)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(41, 20)
        Me.TextBox2.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(64, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(257, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = " health condition make     percent of all possible ones"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label2.Location = New System.Drawing.Point(109, 78)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(14, 13)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "k"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label3.Location = New System.Drawing.Point(109, 116)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(14, 13)
        Me.Label3.TabIndex = 5
        Me.Label3.Text = "d"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.Color.Transparent
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label4.Location = New System.Drawing.Point(175, 23)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(14, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "d"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(178, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label5.Location = New System.Drawing.Point(52, 23)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(14, 13)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "k"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(338, 266)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Button1)
        Me.Name = "Form1"
        Me.Text = "Produce HealthConditions Randomly"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region


    Private Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim ioline As String
        Dim tuples(123595) As String
        Dim i As Integer = 1
        'read up
        Dim arraysize As Integer
        Dim oWatch As New Stopwatch
        oWatch.Start()
        Dim strArr() As String
        Dim str As String
        Try
            Dim oFile As FileStream = New FileStream(Environment.CurrentDirectory & "\in.txt", FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite)
            Dim oReader As StreamReader = New StreamReader(oFile)
            ioline = oReader.ReadLine

            Str = ioline
            strArr = str.Split
            ioline = strArr(0) & " " & strArr(1)
            tuples(0) = ioline
            Do Until ioline Is Nothing
                ioline = oReader.ReadLine
                str = ioline
                strArr = str.Split
                ioline = strArr(0) & " " & strArr(1)
                tuples(i) = ioline
                i = i + 1
            Loop
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        arraysize = i - 1
        'produce and attach health conditions
        Dim hc() As String = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j"}
        Static r As Random
        Dim seed() As Byte = New Byte(3) {}
        Dim k_in As Integer = CInt(Me.TextBox1.Text)
        Dim d_in As Integer = CInt(Me.TextBox2.Text)
        Dim counthc(9) As Integer
        Dim totalcount As Integer
        For i = 0 To k_in - 1
            counthc(i) = CInt(arraysize * (d_in / 100) / k_in)
            totalcount = totalcount + counthc(i)
        Next i
        Dim remain As Integer
        remain = CInt(Math.Floor(arraysize * (d_in / 100))) - totalcount
        For i = 0 To remain - 1
            counthc(i) = counthc(i) + 1
        Next i

        remain = arraysize - CInt(Math.Floor(arraysize * (d_in / 100)))
        Dim rng As New RNGCryptoServiceProvider()
        For i = k_in To 9
            counthc(i) = CInt(Math.Floor(remain / (10 - k_in)))
        Next i
        'if the sum of counts is less than arraysize add to the last one
        Dim all As Integer
        For i = 0 To 9
            all = all + counthc(i)
        Next i
        For i = k_in To k_in + (arraysize - all) - 1
            counthc(i) = counthc(i) + 1
        Next i
        Dim index As Integer
        For i = 0 To arraysize - 1
            rng.GetBytes(seed)
            r = New Random(BitConverter.ToInt32(seed, 0))
            index = r.Next(0, 10)
            While counthc(index) = 0
                index = index + 1
                If index = 10 Then index = 0
            End While
            tuples(i) = tuples(i) & " " & hc(index)
            counthc(index) = counthc(index) - 1
        Next i
        'write down 
        Try
            Dim oFile As FileStream = New FileStream(Environment.CurrentDirectory & "\out" & CStr(k_in) & " _ " & CStr(d_in) & ".txt", FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite)
            Dim iWriter As StreamWriter = New StreamWriter(oFile)
            For i = 0 To arraysize - 1
                iWriter.WriteLine(tuples(i))
            Next i

            iWriter.Close()
            oFile.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        oWatch.Stop()
        MsgBox("Total Time Taken (Including I/O operations) := " & oWatch.ElapsedMilliseconds.ToString)
        MessageBox.Show("End of Process")
    End Sub
End Class
