Imports System.ComponentModel
Imports System.Drawing
Imports System.Windows.Forms
Imports System.IO
Imports System.Security.Cryptography
Imports System.Diagnostics


Public Class Form1
    Inherits System.Windows.Forms.Form
    'global variables



    Public Sub New()
        MyBase.New()

        Form1 = Me

        'This call is required by the Win Form Designer.
        InitializeComponent()

        'TODO: Add any initialization after the InitializeComponent() call
    End Sub

    'Form overrides dispose to clean up the component list.
    Public Overloads Sub Dispose()
        MyBase.Dispose()
        components.Dispose()
    End Sub

#Region " Windows Form Designer generated code "

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.Container
    Private WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label

    Dim WithEvents Form1 As System.Windows.Forms.Form

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(67, 363)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(184, 33)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Generate Cloak Area"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(229, 26)
        Me.TextBox1.MaxLength = 2
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(51, 20)
        Me.TextBox1.TabIndex = 1
        Me.TextBox1.Text = "5"
        Me.TextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label1.Location = New System.Drawing.Point(50, 33)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(87, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "diversity level ( l )"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label2.Location = New System.Drawing.Point(50, 98)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(123, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Total Number of Groups:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label3.Location = New System.Drawing.Point(50, 157)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(172, 13)
        Me.Label3.TabIndex = 6
        Me.Label3.Text = "Number of Fake Requests Needed"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label4.Location = New System.Drawing.Point(50, 124)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(135, 13)
        Me.Label4.TabIndex = 8
        Me.Label4.Text = " Number of Merged Groups"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(251, 98)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(0, 13)
        Me.Label5.TabIndex = 9
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(251, 124)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(0, 13)
        Me.Label6.TabIndex = 10
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(251, 157)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(0, 13)
        Me.Label7.TabIndex = 11
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label8.Location = New System.Drawing.Point(50, 186)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(111, 13)
        Me.Label8.TabIndex = 12
        Me.Label8.Text = "Average Area of MBR"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(251, 186)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(0, 13)
        Me.Label9.TabIndex = 13
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(251, 259)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(0, 13)
        Me.Label10.TabIndex = 14
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(251, 284)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(0, 13)
        Me.Label11.TabIndex = 15
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label12.Location = New System.Drawing.Point(50, 259)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(88, 13)
        Me.Label12.TabIndex = 16
        Me.Label12.Text = "Min Area of MBR"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label13.Location = New System.Drawing.Point(50, 284)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(91, 13)
        Me.Label13.TabIndex = 17
        Me.Label13.Text = "Max Area of MBR"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.ForeColor = System.Drawing.Color.Red
        Me.Label14.Location = New System.Drawing.Point(50, 312)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(64, 13)
        Me.Label14.TabIndex = 19
        Me.Label14.Text = "Time Taken"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(251, 312)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(0, 13)
        Me.Label15.TabIndex = 18
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(251, 222)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(0, 13)
        Me.Label16.TabIndex = 21
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label17.Location = New System.Drawing.Point(50, 222)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(83, 13)
        Me.Label17.TabIndex = 20
        Me.Label17.Text = "Total MBR Area"
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(229, 61)
        Me.TextBox2.MaxLength = 2
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(51, 20)
        Me.TextBox2.TabIndex = 25
        Me.TextBox2.Text = "2"
        Me.TextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.ForeColor = System.Drawing.SystemColors.ActiveCaption
        Me.Label18.Location = New System.Drawing.Point(50, 64)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(168, 13)
        Me.Label18.TabIndex = 24
        Me.Label18.Text = "merge when groupsize is less than"
        '
        'Form1
        '
        Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
        Me.ClientSize = New System.Drawing.Size(341, 421)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Button1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Form1"
        Me.Text = "Grouping Algorithm  with Hilbert Optimization"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

#End Region
    '---------------------------------------
    Private Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
        Dim oWatch As New Stopwatch
        Dim ioline As String
        Dim i As Integer = 0
        Dim j As Integer = 0
        Dim c As Integer = 0
        Dim k As Integer = 0
        Dim s As String = ""
        Dim strArr() As String
        Dim str As String
        Dim hc As String
        Dim fake As Integer = 0
        Dim t(9)() As String
        Dim count(9) As Integer
        Static l As Integer 'l is diversity level
        l = CInt(Me.TextBox1.Text)
        Dim anonymizegroup(123593, 2 * l - 2) As String
        Dim incomp As Integer = 123593
        'read up

        Try
            Dim oFile As FileStream = New FileStream(Environment.CurrentDirectory & "\in.txt", FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite)
            Dim oReader As StreamReader = New StreamReader(oFile)
            For i = 0 To 9
                count(i) = 0
            Next i
            For i = 0 To 9
                t(i) = New String(123593) {}
            Next i
            ioline = "non-empty"   'enetering the do..until loop for the first time
            Do Until ioline Is Nothing
                ioline = oReader.ReadLine
                If ioline Is Nothing Then
                    GoTo 1
                End If
                str = ioline
                strArr = str.Split
                hc = strArr(2)
                Select Case hc
                    Case "a"
                        i = 0
                    Case "b"
                        i = 1
                    Case "c"
                        i = 2
                    Case "d"
                        i = 3
                    Case "e"
                        i = 4
                    Case "f"
                        i = 5
                    Case "g"
                        i = 6
                    Case "h"
                        i = 7
                    Case "i"
                        i = 8
                    Case "j"
                        i = 9
                End Select
                t(i)(count(i)) = str
                count(i) = count(i) + 1
            Loop
1:
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        'sort each health condition group members according to their hilbert value in ascending order
        Dim temp(123593) As String
        Dim Arr() As String
        Dim rec As String
        Dim hilbert As Double
        Dim hilbert2 As Double
        Dim acount As Integer = 0 'index for anonmized groups
        '----------------------Start Time Measuring---------------------------
        oWatch.Start()
        For j = 0 To 9
            For i = 0 To count(j) - 1
                For k = 0 To count(j) - i - 2
                    rec = t(j)(k)
                    Arr = rec.Split
                    hilbert = CDbl(Arr(3))
                    rec = t(j)(k + 1)
                    Arr = rec.Split
                    hilbert2 = CDbl(Arr(3))
                    If hilbert > hilbert2 Then
                        '   swap()
                        Dim tempstr As String
                        tempstr = t(j)(k)
                        t(j)(k) = t(j)(k + 1)
                        t(j)(k + 1) = tempstr

                    End If
                Next k
            Next i
        Next j
        'sort all groups according to the hilbert value of their last  member in ascending order����� ����� ����� ��� 
        Dim tem As Integer
        '    =========================================

        For j = 0 To 9
            For k = 0 To 9 - j - 1
                If count(k) = 0 Then
                    hilbert = 0
                Else
                    If t(k)(count(k) - 1) = "" Then
                        hilbert = 0
                    Else
                        rec = t(k)(count(k) - 1)
                        Arr = rec.Split
                        hilbert = CDbl(Arr(3))
                    End If
                End If
                If count(k + 1) = 0 Then
                    hilbert2 = 0
                Else
                    If t(k + 1)(count(k + 1) - 1) = "" Then
                        hilbert2 = 0
                    Else
                        rec = t(k + 1)(count(k + 1) - 1)
                        Arr = rec.Split
                        hilbert2 = CDbl(Arr(3))
                    End If
                End If
                If hilbert > hilbert2 Then
                    'swap()
                    t(k).CopyTo(temp, 0)
                    t(k + 1).CopyTo(t(k), 0)
                    temp.CopyTo(t(k + 1), 0)
                    tem = count(k)
                    count(k) = count(k + 1)
                    count(k + 1) = tem
                End If
            Next k
        Next j
        Dim tempcount(9) As Integer
        Dim ttem(9)() As String
        For i = 0 To 9
            ttem(i) = New String(123593) {}
        Next i
        For i = 0 To 9
            tempcount(i) = 0
        Next i
        For i = 0 To 9
            For j = 0 To 123593
                ttem(i)(j) = Nothing
            Next j
        Next i
        j = 0
        For i = 0 To 9
            If count(i) <> 0 Then
                tempcount(j) = count(i)
                t(i).CopyTo(ttem(j), 0)
                j = j + 1
            End If
        Next i
        tempcount.CopyTo(count, 0)
        For i = 0 To 9
            ttem(i).CopyTo(t(i), 0)
        Next i
        '   ===================
        '   write down the result so that multiple computations become faster
        'Try
        '    Dim oFile As FileStream = New FileStream(Environment.CurrentDirectory & "\t.txt", FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite)
        '    Dim iWriter As StreamWriter = New StreamWriter(oFile)
        '    For i = 0 To 9
        '        For j = 0 To 123593
        '            iWriter.WriteLine(t(i)(j))
        '        Next j
        '    Next i
        '    iWriter.Close()
        '    oFile.Close()
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message)
        'End Try
        ''   ============
        'Try
        '    Dim oFile As FileStream = New FileStream(Environment.CurrentDirectory & "\count.txt", FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite)
        '    Dim iWriter As StreamWriter = New StreamWriter(oFile)
        '    For i = 0 To 9
        '        iWriter.WriteLine(count(i))
        '    Next i
        '    iWriter.Close()
        '    oFile.Close()
        '    MessageBox.Show("t and count have been written")
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message)
        'End Try
        ''fetch the value of t array and acount from appropriate files
        'Try
        '    Dim oFile As FileStream = New FileStream(Environment.CurrentDirectory & "\t.txt", FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite)
        '    Dim oReader As StreamReader = New StreamReader(oFile)
        '    ioline = "non-empty"   'enetering the do..until loop for the first time
        '    For i = 0 To 9
        '        For j = 0 To 123593
        '            ioline = oReader.ReadLine
        '            t(i)(j) = ioline
        '        Next j
        '    Next i
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message)
        'End Try
        'Try
        '    Dim oFile As FileStream = New FileStream(Environment.CurrentDirectory & "\count.txt", FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite)
        '    Dim oReader As StreamReader = New StreamReader(oFile)
        '    ioline = "non-empty"   'enetering the do..until loop for the first time
        '    For i = 0 To 9
        '        ioline = oReader.ReadLine
        '        count(i) = CInt(ioline)
        '    Next i
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message)
        'End Try
        Dim lastgroupremnants As Integer = 0
top:
        ' find the first non-empty group
        Dim test As Integer = 1
        For i = 0 To 9
            If count(i) > 0 Then
                test = 0
                Exit For
            End If
            test = 1
        Next i
        Dim currentgroup As Integer
        Dim nextgroup As Integer
        Dim groupsize As Integer
        Dim o As Integer = 0
        Dim p As Integer = 0

        currentgroup = i
        If i = 10 Then GoTo the_end
        groupsize = count(i)
        If test = 1 Then 'there is no non-empty group
            GoTo the_end
        End If
        'composing a new anonymized group
        'add the first member
        For j = 0 To groupsize
            If count(currentgroup) = 0 Then
                GoTo top
            End If
            anonymizegroup(acount, 0) = t(currentgroup)(count(currentgroup) - 1)
            count(currentgroup) = count(currentgroup) - 1
            'find out the other l-1 members of the new composing anonymized group
            If currentgroup = 9 Then
                If lastgroupremnants = 0 And incomp = 123593 Then
                    lastgroupremnants = count(9)
                End If
            End If
            nextgroup = currentgroup + 1
            For k = 1 To l - 1
                For i = nextgroup To 9
                    If count(i) = 0 Then
                        If i = 9 Then
                            If incomp = 123593 Then
                                incomp = acount
                            End If
                        End If
                        Continue For
                    End If
                    'pop 
                    anonymizegroup(acount, k) = t(i)(count(i) - 1)
                    count(i) = count(i) - 1
                    nextgroup = i + 1
                    If nextgroup = 10 And k < l - 1 Then
                        If incomp = 123593 Then
                            incomp = acount
                        End If
                    End If
                    'If nextgroup > 9 - l Then
                    '    MsgBox("furthur grouping is not possible")
                    '    GoTo the_end
                    'End If
                    Exit For
                Next i

            Next k
            acount = acount + 1 'now a new anonymized group is composed and the index moves forward
        Next j

        GoTo top
the_end:
        If lastgroupremnants <> 0 Then
            incomp = acount - lastgroupremnants - 1
        End If
        'If incomp < acount / 2 Then
        '    MsgBox("The divesity level is to high and can`t be provided")
        '    GoTo write
        'End If
        'it means that we have composed (acount-1) groups but just incomp of them is compelete and the others are not
        ' in this stage we try to increase the number of compelet groups to (acount-1)/2 so that
        ' at least half of the composed groups contain l member
        ' we do this by adding fake request
        ' the value of hc for fake requests is choosen randomly from the values that are not added before
        ' their x, y should fall into the current cloak area so we put x,y of the first member of group as all fake requests` x,y

        'MsgBox("The divesity level is to high and can`t be provided")
        'pre-merging steps
        If incomp = 123593 Then incomp = acount
        'for l=1 
        If l = 1 Then
            incomp = acount
            Me.Label6.Text = "0"
            GoTo down
        End If
        Dim hcval As Integer
        Dim newhc As String = ""
        Dim hcarr(9) As Integer
        Dim newincomp As Integer
        Dim leastgroupsize As Integer
        leastgroupsize = CInt(TextBox2.Text)
        Dim semigroupsize As Integer = 0
        For i = incomp To acount
            For j = 0 To 2 * l - 2
                If anonymizegroup(i, j) <> Nothing Then
                    semigroupsize = semigroupsize + 1
                End If
            Next j
            If semigroupsize <= leastgroupsize Then
                newincomp = i
                Exit For
            End If
            semigroupsize = 0
        Next i
        'group should complete to a l-diverse one
        If incomp = newincomp Then GoTo pre
        For i = incomp To newincomp - 1
            Dim dd As Integer
            Dim jj As Integer
            For dd = 0 To 9
                hcarr(dd) = 0
            Next dd
            For jj = 0 To 2 * l - 2
                If anonymizegroup(i, jj) = Nothing Then
                    Continue For
                End If
                str = anonymizegroup(i, jj)
                strArr = str.Split
                hc = strArr(2)
                Select Case hc
                    Case "a"
                        hcval = 0
                    Case "b"
                        hcval = 1
                    Case "c"
                        hcval = 2
                    Case "d"
                        hcval = 3
                    Case "e"
                        hcval = 4
                    Case "f"
                        hcval = 5
                    Case "g"
                        hcval = 6
                    Case "h"
                        hcval = 7
                    Case "i"
                        hcval = 8
                    Case "j"
                        hcval = 9
                End Select
                hcarr(hcval) = hcarr(hcval) + 1
            Next jj
            Dim anonymmember As Integer = 0
            Dim f As Integer
            For f = 0 To 9
                anonymmember = anonymmember + hcarr(f)
            Next f
            f = 0
            While f < l - anonymmember
                Dim xy As String
                'what if the group is completly empty?? it`s not possible since we are considering incomplete groups not empty ones!!!
                str = anonymizegroup(i, 0)
                strArr = str.Split
                xy = strArr(0) & " " & strArr(1)
                'find a new hc
                Dim z As Integer
                For z = 0 To 9
                    If hcarr(z) = 0 Then
                        Select Case z
                            Case 0
                                newhc = "a"
                            Case 1
                                newhc = "b"
                            Case 2
                                newhc = "c"
                            Case 3
                                newhc = "d"
                            Case 4
                                newhc = "e"
                            Case 5
                                newhc = "f"
                            Case 6
                                newhc = "g"
                            Case 7
                                newhc = "h"
                            Case 8
                                newhc = "i"
                            Case 9
                                newhc = "j"
                        End Select
                        hcarr(z) = hcarr(z) + 1
                        Exit For
                    End If
                Next z
                anonymizegroup(i, anonymmember + f) = xy & " " & newhc
                fake = fake + 1
                f = f + 1
            End While
            incomp = incomp + 1
        Next i
        'incomp= index of the first incomplete group (in other word, it`s the number of compelete groups)
pre:
        If incomp < acount / 2 Then
            Dim ceil As Integer
            ceil = CInt(Math.Ceiling((acount - 1) / 2))
            For i = incomp To ceil
                Dim dd As Integer
                For dd = 0 To 9
                    hcarr(dd) = 0
                Next dd
                For j = 0 To 2 * l - 2
                    If anonymizegroup(i, j) = Nothing Then
                        Continue For
                    End If
                    str = anonymizegroup(i, j)
                    strArr = str.Split
                    hc = strArr(2)
                    Select Case hc
                        Case "a"
                            hcval = 0
                        Case "b"
                            hcval = 1
                        Case "c"
                            hcval = 2
                        Case "d"
                            hcval = 3
                        Case "e"
                            hcval = 4
                        Case "f"
                            hcval = 5
                        Case "g"
                            hcval = 6
                        Case "h"
                            hcval = 7
                        Case "i"
                            hcval = 8
                        Case "j"
                            hcval = 9
                    End Select
                    hcarr(hcval) = hcarr(hcval) + 1
                Next j
                Dim anonymmember As Integer = 0
                Dim f As Integer
                For f = 0 To 9
                    anonymmember = anonymmember + hcarr(f)
                Next f
                f = 0
                While f < l - anonymmember
                    Dim xy As String
                    'what if the group is completly empty?? it`s not possible since we are considering incomplete groups not empty ones!!!
                    str = anonymizegroup(i, 0)
                    strArr = str.Split
                    xy = strArr(0) & " " & strArr(1)
                    'find a new hc
                    Dim z As Integer
                    For z = 0 To 9
                        If hcarr(z) = 0 Then
                            Select Case z
                                Case 0
                                    newhc = "a"
                                Case 1
                                    newhc = "b"
                                Case 2
                                    newhc = "c"
                                Case 3
                                    newhc = "d"
                                Case 4
                                    newhc = "e"
                                Case 5
                                    newhc = "f"
                                Case 6
                                    newhc = "g"
                                Case 7
                                    newhc = "h"
                                Case 8
                                    newhc = "i"
                                Case 9
                                    newhc = "j"
                            End Select
                            hcarr(z) = hcarr(z) + 1
                            Exit For
                        End If
                    Next z
                    anonymizegroup(i, anonymmember + f) = xy & " " & newhc
                    fake = fake + 1
                    f = f + 1
                End While
                incomp = incomp + 1
            Next i

        End If
        Me.Label6.Text = CStr(acount - incomp)
        'merging

        'for l=1 
        If l = 1 Then
            incomp = acount
            Me.Label6.Text = "0"
            GoTo down
        End If
        For i = incomp To acount
            p = 0
            For j = 0 To l - 1
                If anonymizegroup(i, j) <> Nothing Then
                    anonymizegroup(o, l + p) = anonymizegroup(i, j)
                    anonymizegroup(i, j) = Nothing
                    p = p + 1
                End If
            Next j
            o = o + 1
            acount = acount - 1
        Next i
        acount = acount + 1
down:
        Me.Label5.Text = CStr(acount)
        '---------------------compute number of fake requests-------
        Dim rc As String
        Dim hcstr As String
        Dim recstr(3) As String
        Dim hcarray(9) As Integer
        Dim num As Integer = 0
        Dim h As Integer
        For i = 0 To incomp
            Dim state As Integer
            Dim hcmax As Integer
            For j = 0 To 2 * l - 2
                rc = anonymizegroup(i, j)
                If rc = Nothing Then Exit For
                recstr = rc.Split
                hcstr = recstr(2)
                Select Case hcstr
                    Case "a"
                        state = 0
                    Case "b"
                        state = 1
                    Case "c"
                        state = 2
                    Case "d"
                        state = 3
                    Case "e"
                        state = 4
                    Case "f"
                        state = 5
                    Case "g"
                        state = 6
                    Case "h"
                        state = 7
                    Case "i"
                        state = 8
                    Case "j"
                        state = 9
                End Select
                hcarray(state) = hcarray(state) + 1
            Next j
            Dim u As Integer
            hcmax = hcarray(0)
            For u = 1 To 9
                If hcarray(u) > hcmax Then
                    hcmax = hcarray(u)
                End If
            Next u
            For u = 0 To 9
                If hcarray(u) = hcmax Then
                    num = num + 1
                End If
            Next u
            If num < l Then
                fake = fake + (l - num)
            End If
            For h = 0 To 9
                hcarray(h) = 0
            Next h
            hcmax = 0
            num = 0
        Next i
        Me.Label7.Text = CStr(fake)
mbr:
        'find(mbr)
        Dim x As Single
        Dim y As Single
        Dim xmax As Single = 0
        Dim ymax As Single = 0
        Dim xmin As Single = 10000
        Dim ymin As Single = 10000
        Dim mbr(acount - 1) As Single
        Dim r As Integer = 0
        For i = 0 To acount - 1
            For j = 0 To 2 * l - 2
                rc = anonymizegroup(i, j)
                If rc = Nothing Then Exit For
                recstr = rc.Split
                x = CSng(recstr(0))
                If x > xmax Then
                    xmax = x
                End If
                If x < xmin Then
                    xmin = x
                End If
                y = CSng(recstr(1))
                If y > ymax Then
                    ymax = y
                End If
                If y < ymin Then
                    ymin = y
                End If
            Next j
            mbr(r) = (xmax - xmin) * (ymax - ymin)
            xmax = 0
            ymax = 0
            xmin = 10000
            ymin = 10000
            r = r + 1
        Next i
        Dim avgmbr As Single
        For i = 0 To acount - 1
            avgmbr = avgmbr + mbr(i)
        Next i
        Dim minmbr As Single
        Dim maxmbr As Single
        minmbr = mbr(0)
        maxmbr = minmbr
        For i = 1 To acount - 1
            If mbr(i) < minmbr Then
                minmbr = mbr(i)
            ElseIf mbr(i) > maxmbr Then
                maxmbr = mbr(i)
            End If
        Next i
        Me.Label10.Text = CStr(minmbr)
        Me.Label11.Text = CStr(maxmbr)
        Label16.Text = CStr(avgmbr)
        avgmbr = avgmbr / (acount * 10000)
        Me.Label9.Text = CStr(avgmbr)
        ''write down the result (anonymized groups)
        '------------------------Stop Time Measuring-------------
        oWatch.Stop()
        Label15.Text = oWatch.ElapsedMilliseconds.ToString
write:
        Try
            Dim oFile As FileStream = New FileStream(Environment.CurrentDirectory & "\anon.txt", FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite)
            Dim iWriter As StreamWriter = New StreamWriter(oFile)
            For i = 0 To acount - 1
                iWriter.WriteLine("----------" & "group no.=" & i & "----------")
                For j = 0 To 2 * l - 2
                    iWriter.WriteLine(anonymizegroup(i, j))
                Next j
            Next i
            iWriter.Close()
            oFile.Close()
            MessageBox.Show("End of Process")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

End Class
